# PVC Reporting Improvement (High Noise)

ECR: 000025

## Problem

PVC count is turned off if the noise duration is greater than 4% of total patient monitoring time.

## Requested Action

Develop a method to count PVCs only when noise is not present in the ECG strip. This will allow for a PVC count to be generated for patients that currently have PVC count turned off.

## Engineering Plan

1. Design and document a solution (Josh and Justin)
2. Verify the solution removes PVC count in regions of high noise (Josh and Justin)
3. Submit JIRA ticket when solution is ready (Josh and Justin)

## API

To be determined.